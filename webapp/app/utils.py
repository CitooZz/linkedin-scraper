from xlwt import XFStyle
from bs4 import BeautifulSoup
import requests
import sys
import html5lib


class ExcelWriter(object):

    def __init__(self, sheet):
        self.sheet = sheet
        self._row = 0

    def writerow(self, row):
        for cell, data in enumerate(row):

            if data is None:
                data = ''

            style = XFStyle()
            style.alignment.wrap = 1
            self.sheet.write(self._row, cell, data, style)

        self._row += 1


def parse_person(client, keyword, page=1):
    results = []
    resp = client.get(
        'https://www.linkedin.com/vsearch/fj?keywords=%s&pt=people&page_num=%s' % (keyword, page)).json()

    for result in resp['content']['page']['voltron_unified_search_json']['search']['results']:
        try:
            person = result['person']['degree_result_person']

            company_exist = True
            profile_link = result['person'].get('link_nprofile_view_4', None)

            if profile_link is None:
                profile_link = result['person'].get('link_nprofile_view_headless')
                company_exist = False 

            temp_data = {'first_name': person['fNameP'], 'last_name': person[
                'lNameP'], 'title': result['person']['fmt_headline'], 'profile_link': profile_link, 
                'address': result['person']['fmt_location'], 'current_company': ''}

            
            if profile_link and company_exist:
                detail_page = client.get(profile_link).content

                soup = BeautifulSoup(detail_page, 'html5lib')
                company_elements = soup.select('#overview-summary-current a')[-1:]
                if len(company_elements) == 1:
                    temp_data['current_company'] = company_elements[0].string
            
            results.append(temp_data)

        except KeyError:
            pass

    return results


def linkedin_login(username, password):
    client = requests.Session()
    HOMEPAGE_URL = 'https://www.linkedin.com'
    LOGIN_URL = 'https://www.linkedin.com/uas/login-submit'

    html = client.get(HOMEPAGE_URL).content
    soup = BeautifulSoup(html, "html5lib")
    csrf = soup.find(id="loginCsrfParam-login")['value']

    login_information = {
        'session_key': username,
        'session_password': password,
        'loginCsrfParam': csrf,
    }

    login_resp = client.post(LOGIN_URL, data=login_information)
    return client
