from django.db import models

# Create your models here.


class Search(models.Model):
    pharse = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.pharse


class Result(models.Model):
    search = models.ForeignKey(Search, related_name="results")

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    title = models.CharField(max_length=100, null=True, blank=True)
    current_company = models.CharField(max_length=100, null=True, blank=True)
    profile_link = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name)
