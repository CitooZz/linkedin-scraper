from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.conf import settings
from app.models import Search, Result
from app.utils import ExcelWriter, linkedin_login, parse_person
from xlwt import Workbook
import csv
import StringIO


def search(request):

    context = {}
    if request.POST.get('keyword') and request.POST.get('email') and request.POST.get('password'):
        keyword = request.POST.get('keyword')
        email = request.POST.get('email')
        password = request.POST.get('password')
        search = Search.objects.create(pharse=keyword)
        page = request.POST.get('page')

        client = linkedin_login(email, password)
        
        results = parse_person(client, keyword, page)
        for result in results:
            result['search'] = search
            Result.objects.create(**result) 

        context['results'] = Result.objects.filter(search=search)
        context['search'] = search
        context['keyword'] = keyword
        context['page'] = page
        context['email'] = email
        context['password'] = password

    return render(request, "app/home.html", context)


def search_print(request, search_id):
    search = get_object_or_404(Search, id=search_id)
    results = Result.objects.filter(search=search)

    context = {
        'search': search,
        'results': results
    }
    return render(request, 'app/search_print.html', context)


def search_export_csv(request, search_id):
    search = get_object_or_404(Search, id=search_id)
    results = Result.objects.filter(search=search)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = "attachment; filename=search-results.csv"
    csv_writer = csv.writer(response)

    header = ['Firstname', 'Lastname', 'Title', 'Current Company', 'Address', 'Profile Link']
    csv_writer.writerow(header)

    for result in results:
        data = [
            result.first_name,
            result.last_name,
            result.title if result.title else '',
            result.current_company if result.current_company else '',
            result.address if result.address else '',
            result.profile_link if result.profile_link else ''
        ]

        csv_writer.writerow(data)

    return response


def search_export_excel(request, search_id):
    search = get_object_or_404(Search, id=search_id)
    results = Result.objects.filter(search=search)

    book = Workbook(style_compression=2)
    sheet = book.add_sheet('Search Results')
    writer = ExcelWriter(sheet)

    header = ["Firstname", "Lastname", "Title", "Current Company", 'Address', 'Profile Link']
    writer.writerow(header)

    for result in results:
        data = [
            result.first_name,
            result.last_name,
            result.title if result.title else '',
            result.current_company if result.current_company else '',
            result.address if result.address else '',
            result.profile_link if result.profile_link else ''
        ]

        writer.writerow(data)

    out = StringIO.StringIO()
    book.save(out)
    response = HttpResponse(
        out.getvalue(), content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = 'attachment; filename=search_result.xls'

    return response
